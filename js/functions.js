$(window).scroll(function(){

 var iScroll = $(this).scrollTop();

$('.logo').css({
      'transform' : 'translate(0px, '+ iScroll /2 +'%)'
    });

    $('.back-bird').css({
      'transform' : 'translate(0px, '+ iScroll /4 +'%)'
    });

    $('.fore-bird').css({
      'transform' : 'translate(0px, -'+ iScroll /40 +'%)'
    });
 
 if(iScroll > $('.clothes-pics').offset().top - ($(window).height() / 1.2)){
 	
 	$('.clothes-pics figure').each(function(i){
   
    setTimeout(function(){
    $('.clothes-pics figure').eq(i).addClass('is-showing');
    }, 150 * (i+1));

 	});
 }

if(iScroll > $('.large-window').offset().top - $(window).height()){

    $('.large-window').css({'background-position':'center '+ (iScroll - $('.large-window').offset().top) +'px'});

    var opacity = (iScroll - $('.large-window').offset().top + 400) / (iScroll / 5);

    $('.window-tint').css({'opacity': opacity});

  }
  if(iScroll > $('.large-windows').offset().top - $(window).height()){

    $('.large-windows').css({'background-position':'center '+ (iScroll - $('.large-windows').offset().top) +'px'});

    var opacity = (iScroll - $('.large-windows').offset().top + 400) / (iScroll / 5);

    $('.window-tint').css({'opacity': opacity});

  }

  if(iScroll > $('.blog-posts').offset().top - $(window).height()){

    var offset = (Math.min(0, iScroll - $('.blog-posts').offset().top +$(window).height() - 350)).toFixed();

    $('.post-1').css({'transform': 'translate('+ offset +'px, '+ Math.abs(offset * 0.2) +'px)'});

    $('.post-3').css({'transform': 'translate('+ Math.abs(offset) +'px, '+ Math.abs(offset * 0.2) +'px)'});

  }
   if(iScroll > $('.clothes-pic').offset().top - $(window).height()){

    var offset = (Math.min(0, iScroll - $('.clothes-pic').offset().top +$(window).height() - 350)).toFixed();

    $('.one').css({'transform': 'translate('+ offset +'px, '+ Math.abs(offset * 0.2) +'px)'});

    $('.three').css({'transform': 'translate('+ Math.abs(offset) +'px, '+ Math.abs(offset * 0.2) +'px)'});
  }


  if(iScroll > $('.blog-post').offset().top - $(window).height()){

    var offset = (Math.min(0, iScroll - $('.blog-post').offset().top +$(window).height() - 350)).toFixed();

    $('.post-1').css({'transform': 'translate('+ offset +'px, '+ Math.abs(offset * 0.2) +'px)'});

    $('.post-3').css({'transform': 'translate('+ Math.abs(offset) +'px, '+ Math.abs(offset * 0.2) +'px)'});

  }
   
});